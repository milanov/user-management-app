package io.milanov.user_management_app.user.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;

import javax.inject.Inject;
import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.milanov.user_management_app.user.User;
import io.milanov.user_management_app.user.UserRepository;

/**
 * REST controller for managing User(s).
 */
@RestController
@RequestMapping("/users")
public class UserResource {

	@Inject
	private UserRepository userRepository;

	/**
	 * POST /users -> create a new user.
	 */
	@RequestMapping(value = "", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<User> create(@Valid @RequestBody User user) throws URISyntaxException {
		if (user.getId() != null) {
			return ResponseEntity.badRequest().body(null);
		}
		User result = userRepository.save(user);

		return ResponseEntity.created(new URI("/users/" + user.getId())).body(result);
	}

	/**
	 * PUT /users -> update an existing user.
	 */
	@RequestMapping(value = "", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<User> update(@Valid @RequestBody User user) {
		if (user.getId() == null) {
			return ResponseEntity.badRequest().body(null);
		}
		User result = userRepository.save(user);

		return ResponseEntity.ok().body(result);
	}

	/**
	 * GET /users -> get all the users.
	 */
	@RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Page<User> getAll(Pageable pageable) {
		return userRepository.findAll(pageable);
	}

	/**
	 * GET /users/:id -> get specific user.
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<User> get(@PathVariable Long id) {
		return Optional.ofNullable(userRepository.findOne(id))
				.map(user -> new ResponseEntity<>(user, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	/**
	 * DELETE /users/:id -> delete specific user.
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public void delete(@PathVariable Long id) {
		userRepository.delete(id);
	}
}
