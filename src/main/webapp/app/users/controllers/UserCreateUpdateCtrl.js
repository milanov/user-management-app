(function(angular) {
	angular.module('userApp').controller('UserCreateUpdateCtrl',
		['$scope', '$modalInstance', 'UserService', function ($scope, $modalInstance, UserService) {

		$scope.cancel = function () {
			$modalInstance.dismiss('cancel');
		};

		$scope.status = {
			opened: false
		};

		$scope.open = function($event) {
			$scope.status.opened = true;
		};

		$scope.save = function () {
			UserService.saveOrUpdate($scope.user).then(function (user) {
				$scope.refreshUsersTable();
				$scope.cancel();
			});
		}
	}]);
}(angular));
