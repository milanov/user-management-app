(function(angular) {
	angular.module('userApp').controller('UserListCtrl', 
		['$scope', '$modal', '$location', 'ngTableParams', 'UserService', function ($scope, $modal, $location, ngTableParams, UserService) {

		$scope.usersTable = new ngTableParams(angular.extend({
			page: 1,
			count: 10
		}, $location.search()), {
			total: 0,
			getData: function($defer, params) {
				$location.search(params.url()); // put params in url

				UserService.list(params.page(), params.count(), params).then(function(data) {
					params.total(data.totalElements);
					$defer.resolve(data.content);
				});
			}
		});

		$scope.openEdit = function(user) {
			$scope.user = angular.copy(user);
			$modal.open({
				templateUrl: 'app/users/partials/users.detail.html',
				controller: 'UserCreateUpdateCtrl',
				scope: $scope
			});
		}

		$scope.openDelete = function(user) {
			$scope.user = angular.copy(user);
			$modal.open({
				templateUrl: 'app/users/partials/confirm.dialog.html',
				controller: 'UserDeleteCtrl',
				scope: $scope
			});
		}

		$scope.refreshUsersTable = function() {
			$scope.usersTable.reload();
		}
	}]);
}(angular));