(function(angular) {
	angular.module('userApp').controller('UserDeleteCtrl',
		['$scope', '$route', '$modalInstance', 'UserService', function ($scope, $route, $modalInstance, UserService) {

		$scope.cancel = function () {
			$modalInstance.dismiss('cancel');
		};

		$scope.ok = function () {
			UserService.delete($scope.user).then(function () {
				$scope.refreshUsersTable();
				$scope.cancel();
			});
		}
	}]);
}(angular));
