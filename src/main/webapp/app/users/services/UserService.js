(function(angular) {
	angular.module('userApp').factory('UserService', ['$resource', function ($resource) {
		var User = $resource('/users/:id', {}, {
			'query' : { method: 'GET', isArray: false },
			'update': { method: 'PUT'},
			'delete': { method: 'DELETE', params: {
				id:"@id"
			}}
		});

		function listUsers(pageNumber, pageSize, params) {
			var order = params.orderBy()[0];
			if (order) {
				var direction = order[0] == '+' ? 'asc' : 'desc';
				var sortParam = order.substring(1) + ',' + direction;
			}

			return User.query({page: pageNumber - 1, size: pageSize, sort: sortParam }).$promise;
		}

		function saveOrUpdateUser(userData) {
			var user = new User(userData);
			return user.id ? user.$update() : user.$save();
		}

		function deleteUser(userData) {
			var user = new User(userData);
			return user.$delete();
		}

		return {
			list: listUsers,
			saveOrUpdate: saveOrUpdateUser,
			delete: deleteUser
		};
	}]);
}(angular));