/* This directive makes the datepicker popup from angular-bootstrap-ui update
the model with a date string formatted by the given or default formatter and
not the date object itself. */
angular.module('userApp').directive('datepickerPopup', ['$filter', function ($filter){
	return {
		restrict: 'EAC',
		require: 'ngModel',
		link: function(scope, element, attr, ngModel) {
			ngModel.$parsers.push(function toModelValue(date) {
				return $filter('date')(date, attr.datepickerPopup);
			});
		}
	}
}]);