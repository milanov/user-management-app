(function(angular) {
	angular.module('userApp', ['ngResource', 'ngTable', 'ngRoute', 'ui.bootstrap'])
	.config(function ($routeProvider) {
		$routeProvider.
			when('/', {
				controller: 'UserListCtrl',
				templateUrl: 'app/users/partials/users.list.html'
			}).
			otherwise({
				redirectTo: '/'
			});
	});
}(angular));