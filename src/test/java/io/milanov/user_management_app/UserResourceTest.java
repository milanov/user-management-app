package io.milanov.user_management_app;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import io.milanov.user_management_app.user.User;
import io.milanov.user_management_app.user.UserRepository;
import io.milanov.user_management_app.user.rest.UserResource;
import io.milanov.user_management_app.util.TestUtil;

/**
 * Test class for the UserResource REST controller.
 *
 * @see UserResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@Transactional
public class UserResourceTest {
	private static final String USERS_RESOURCE = "/users";
	private static final String USER_RESOURCE = "/users/{id}";
	private static final String USERS_RESOURCE_PAGEABLE = "/users?page=%d&size=%d&sort=%s";

	private static final String FIRST_NAME_JOHN = "John";
	private static final String LAST_NAME_DOE = "Doe";
	private static final String JOHNS_EMAIL = "john@doe.com";
	private static final LocalDate JOHNS_DATE_OF_BIRTH = LocalDate.MIN;

	@Inject
	private UserRepository userRepository;

	private MockMvc restUserMockMvc;

	private User johnDoe;

	@PostConstruct
	public void setup() {
		UserResource userResource = new UserResource();
		ReflectionTestUtils.setField(userResource, "userRepository", userRepository);
		this.restUserMockMvc = MockMvcBuilders.standaloneSetup(userResource)
				.setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver())
				.setViewResolvers(new ViewResolver() {
					@Override
					public View resolveViewName(String viewName, Locale locale) throws Exception {
						return new MappingJackson2JsonView();
					}
				}).build();
	}

	@Before
	public void initTest() {
		johnDoe = new User(FIRST_NAME_JOHN, LAST_NAME_DOE, JOHNS_EMAIL, JOHNS_DATE_OF_BIRTH);
	}

	@Test
	public void createUser() throws Exception {
		int databaseSizeBeforeCreate = (int) userRepository.count();

		// Create a new user
		restUserMockMvc.perform(post(USERS_RESOURCE)
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(johnDoe)))
				.andExpect(status().isCreated());

		// Validate the user in the database
		List<User> users = userRepository.findAll();
		assertThat(users, hasSize(databaseSizeBeforeCreate + 1));

		User john = users.get(users.size() - 1);
		assertThat(john.getFirstName(), is(equalTo(FIRST_NAME_JOHN)));
		assertThat(john.getLastName(), is(equalTo(LAST_NAME_DOE)));
		assertThat(john.getEmail(), is(equalTo(JOHNS_EMAIL)));
		assertThat(john.getDateOfBirth(), is(equalTo(JOHNS_DATE_OF_BIRTH)));
	}

	@Test
	public void createUserWithInvalidEmail() throws Exception {
		User invalidUser = new User("first", "last", "NOT_A_VALID_MAIL", LocalDate.MAX);

		restUserMockMvc.perform(post(USERS_RESOURCE)
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(invalidUser)))
				.andExpect(status().isBadRequest());
	}

	@Test
	public void createUsersWithMissingProperties() throws Exception {
		User userWithoutFirstName = new User(null, LAST_NAME_DOE, JOHNS_EMAIL, JOHNS_DATE_OF_BIRTH);
		User userWithoutLastName = new User(FIRST_NAME_JOHN, null, JOHNS_EMAIL, JOHNS_DATE_OF_BIRTH);
		User userWithoutEmail = new User(FIRST_NAME_JOHN, LAST_NAME_DOE, null, JOHNS_DATE_OF_BIRTH);

		User userWithBlankFirstName = new User(" ", LAST_NAME_DOE, JOHNS_EMAIL, JOHNS_DATE_OF_BIRTH);
		User userWithBlankLastName = new User(FIRST_NAME_JOHN, " ", JOHNS_EMAIL, JOHNS_DATE_OF_BIRTH);
		User userWithBlankEmail = new User(FIRST_NAME_JOHN, LAST_NAME_DOE, " ", JOHNS_DATE_OF_BIRTH);

		List<User> incompleteUsers = Arrays.asList(userWithoutFirstName, userWithoutLastName, userWithoutEmail,
				userWithBlankFirstName, userWithBlankLastName, userWithBlankEmail);

		for (User each : incompleteUsers) {
			restUserMockMvc.perform(post(USERS_RESOURCE)
					.contentType(TestUtil.APPLICATION_JSON_UTF8)
					.content(TestUtil.convertObjectToJsonBytes(each)))
					.andExpect(status().isBadRequest());
		}
	}

	@Test
	public void getSingleUserFromAllUsers() throws Exception {
		userRepository.saveAndFlush(johnDoe);

		restUserMockMvc.perform(get(USERS_RESOURCE))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.content[(@.length-1)].id").value(johnDoe.getId().intValue()))
				.andExpect(jsonPath("$.content[(@.length-1)].firstName").value(FIRST_NAME_JOHN))
				.andExpect(jsonPath("$.content[(@.length-1)].lastName").value(LAST_NAME_DOE))
				.andExpect(jsonPath("$.content[(@.length-1)].email").value(JOHNS_EMAIL))
				.andExpect(jsonPath("$.content[(@.length-1)].dateOfBirth").value(JOHNS_DATE_OF_BIRTH.toString()));
	}

	@Test
	public void testNotGettingAnyResultsForNonexistingPage() throws Exception {
		String secondPageUrl = String.format(USERS_RESOURCE_PAGEABLE, 2, 10, "");
		restUserMockMvc.perform(get(secondPageUrl))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.content", hasSize(0)));
	}

	@Test
	public void getUser() throws Exception {
		userRepository.saveAndFlush(johnDoe);

		// Get a single user
		restUserMockMvc.perform(get(USER_RESOURCE, johnDoe.getId()))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.id").value(johnDoe.getId().intValue()))
				.andExpect(jsonPath("$.firstName").value(FIRST_NAME_JOHN))
				.andExpect(jsonPath("$.lastName").value(LAST_NAME_DOE))
				.andExpect(jsonPath("$.email").value(JOHNS_EMAIL))
				.andExpect(jsonPath("$.dateOfBirth").value(JOHNS_DATE_OF_BIRTH.toString()));
	}

	@Test
	public void getNonExistingUser() throws Exception {
		restUserMockMvc.perform(get(USER_RESOURCE, Long.MAX_VALUE))
				.andExpect(status().isNotFound());
	}

	@Test
	public void updateUser() throws Exception {
		userRepository.saveAndFlush(johnDoe);
		User updatedJohn = new User(johnDoe.getId(), "newJohn", "newDoe", "newjohn@doe.com", LocalDate.MAX);
		int databaseSizeBeforeUpdate = (int) userRepository.count();

		// Update the user
		restUserMockMvc.perform(put("/users")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(updatedJohn)))
				.andExpect(status().isOk());

		// Validate the user in the database
		int databaseSize = (int) userRepository.count();
		assertThat(databaseSize, is(equalTo(databaseSizeBeforeUpdate)));

		User testUser = userRepository.getOne(updatedJohn.getId());
		assertThat(testUser.getFirstName(), is(equalTo("newJohn")));
		assertThat(testUser.getLastName(), is(equalTo("newDoe")));
		assertThat(testUser.getEmail(), is(equalTo("newjohn@doe.com")));
		assertThat(testUser.getDateOfBirth(), is(equalTo(LocalDate.MAX)));
	}

	@Test
	public void deleteUser() throws Exception {
		userRepository.saveAndFlush(johnDoe);
		int databaseSizeBeforeDelete = (int) userRepository.count();

		restUserMockMvc.perform(delete("/users/{id}", johnDoe.getId())
				.accept(TestUtil.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());

		int databaseSize = (int) userRepository.count();
		assertThat(databaseSize, is(equalTo(databaseSizeBeforeDelete - 1)));
	}
}
