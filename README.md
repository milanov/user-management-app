## Synopsis
This is a simple REST-oriented web application that manages user accounts in a database. It is built as a using the `Spring Boot` starter project and `AngularJS` for the frontend. 

## Getting started
The project requires `Maven 3.x` and `JDK 1.8` or above. To build and run the project from the command line navigate to the project directory and execute `mvn spring-boot:run`.

*Side note*: to persist information between runs (currently it's deleted when the app stops) edit `src/main/resources/application.properties` and change `spring.datasource.url` to be equal to `jdbc:hsqldb:file:~/path/to/db` (directory must be writeable).

### Importing into Eclipse
Open Eclipse, select `File -> Import -> Maven -> Existing Maven Project` and navigate to the project directory. In case there is an error with the Java build path right click on the project and select `Properties -> Java Build Path` and under `Libraries` configure the `JRE System Library`.

### Importing into IntelliJ IDEA
Open IntelliJ IDEA, select `File -> Open` and navigate to the project directory. 

## Tests
To run the tests navigate to the project directory and run `mvn test`.